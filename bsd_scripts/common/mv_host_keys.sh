#!/bin/sh -eux

KEY_SRC=/etc/ssh/
KEY_DEST=${KEY_SRC}initial_hostkeys

if [ -d $KEY_DEST ]
then
   rm -rf $KEY_DEST
fi
mkdir $KEY_DEST &&
mv ${KEY_SRC}ssh_host_*_key* $KEY_DEST

#!/bin/sh -eux
head -n1 /etc/motd
pkg_add -Dsnap -Iz python--%3 py3-pip
wpip=$(ls /usr/local/bin/pip* | head -n1)
$wpip install --upgrade pip

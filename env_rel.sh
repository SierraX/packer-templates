OBSDVER=6.3
export P_ISO_CHKSUM=$(curl -s http://ftp.halifax.rwth-aachen.de/openbsd/${OBSDVER}/amd64/SHA256 | awk '/install..\.iso/ { print $4 }')
echo $P_ISO_CHKSUM
